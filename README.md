# Docker image for deploy nginx + php-fpm


## Enviroment
* REPO 
```
REPO="ssh://git@gitlab01.core.irknet.lan:7777/irknet/learn.git"
```
* REPO2
* REPO3
* REPO4
* REPO5
* REPO6


## Introducing
* ubuntu 14.04
* supervisord, how init
* scripts/configure_site.sh


## Deploy

* Configure site's via git repo. You should create and configure nginx.conf. Add this repo how: nginx.conf

```
limit_req_zone      $binary_remote_addr zone=connectreqPerSec200:1m rate=200r/s;
server
{
    server_name            www.learn.irknet.ru learn.irknet.ru;
    root                    /home/www/learn/htdocs;
    listen                  80;
    include                 boilerplate/enable/uploads.conf;
    include                 boilerplate/enable/gzip.conf;
    include                 boilerplate/locations/system.conf;
    include                 boilerplate/locations/errors.conf;
    include                 boilerplate/limits/methods.conf;
    location /
    {
        index index.php index.html index.htm;
    }
    location ~ \.php$
    {
	client_max_body_size    100M;
        include                     boilerplate/fastcgi_params;
        try_files $uri  /index.php?REWRITE_URL=$uri&$args;
        fastcgi_pass            nginx.boilerplate;
        limit_req               zone=connectreqPerSec200 burst=1000 nodelay;
        limit_conn              conPerIp 100;
        fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name; 
        fastcgi_buffers         16 256k; 
        fastcgi_buffer_size     512k;
    }
    access_log              /home/www/learn/logs/access.log main; 
    error_log               /home/www/learn/logs/error.log error;
    limit_req               zone=connectreqPerSec200 burst=1000 nodelay;
    limit_conn              conPerIp 100;
    include                 boilerplate/locations/static.conf;
    set_real_ip_from 94.137.192.0/23;
    set_real_ip_from 10.0.0.0/8;
    set_real_ip_from 127.0.0.1;
    real_ip_header X-Real-IP;
}
```
