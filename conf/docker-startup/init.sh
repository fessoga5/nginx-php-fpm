#!/bin/sh
HOSTNAME=$( cat /etc/hostname )
echo ${HOSTNAME}

FINDHOSTS=$( cat /etc/hosts | grep "${HOSTNAME}" -c )
if [ "$FINDHOSTS" = "0" ]
then
	echo "127.0.0.1 ${HOSTNAME}" >> /etc/hosts
	logger -t startup "Adding ${HOSTNAME} in /etc/hosts"
fi

HOSTNAMEBIN=$( /bin/hostname )

FINDHOSTS=$( cat /etc/hosts | grep "${HOSTNAMEBIN}" -c )
if [ "$FINDHOSTS" = "0" ]
then
	echo "127.0.0.1 ${HOSTNAMEBIN}" >> /etc/hosts
	logger -t startup "Adding ${HOSTNAMEBIN} in /etc/hosts"
fi

FINDHOSTSLOCAL=$( cat /etc/hosts | grep "localhost" -c )
if [ "$FINDHOSTSLOCAL" = "0" ]
then
	echo "127.0.0.1 localhost" >> /etc/hosts
	logger -t startup "Adding localhost in /etc/hosts"
fi
