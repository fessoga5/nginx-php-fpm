#!/usr/bin/env bash
# vim: sts=4 ts=4 sw=4 et ai
CONFIG_GIT_DEFAULT=${REPO:="/opt/emptyindex"}

if [ ! -d /var/www/.ssh ]; then
    mkdir -p -m 0700 /var/www/.ssh
    echo -e "Host *\n\tStrictHostKeyChecking no\n" >> /var/www/.ssh/config

    SSH_KEY="LS0tLS1CRUdJTiBSU0EgUFJJVkFURSBLRVktLS0tLQpNSUlFb3dJQkFBS0NBUUVBd0FGUzNqTFJhNGNXakN2ZE94ZXBWczdQMXNuS1d0NVlmODFjRnpEZ1RGNDhzbzIyCkN4RjE4bVd2UlhaTVNWaHc2b1lzb3NuYmFJSTVMZ3o3aWFjeExuQWRHVXRaZkRsMEtGd0ZwdWZIT0l6dDg3cnEKOEhLRm9qRnJpSnhKdzdpZ3BZZUIvN2RkMEdYTWZ4YktQbTNWSklmcnRjUm9HQ2xuMzM4cXZTMWJMUjhoYUVFZQpPaDdhYzVpOFo0UEdzMUVaQmM4QTRrcDIySmZ2WUhLOWppakI2TnJUZGpTMGZub0JWaWYrUWU1RWJlN1F5a3pOClY3WDRzMTJNREUvVEh6a25vY3gwb0ZQczRueFFsbjVMS0ZqQ05MQU41eXBMUzBnV1lzSGppMlZTQ0F5NmV2bWEKeXZKY0J3aUE0NVF1a1ZWMGU0S0xkaXJnSkxNUEE2WjAxa1dINFFJREFRQUJBb0lCQUNsRDM1QjdzWUIvbnZ2dgpMWFBXeVFFc0g0VVZCMVZ5TmVqNXROSVRJaEducXpuZ3RRQTU5M1JxSURvNGkzcXo5RWlqSDVLNCtWdkRxUkJaCm4vSGNxZmhHQmlyNmc3ZnNEU2ZqWngrUTdzTWJuc1JrSnJ3WWtObmo3eTJFRVdKc0dyS1lwZnJVYU9neUNMcEQKcHFHMkx6blRzSGYzUnp2eUlpM3BnY0Q0cjRESnNZalZxZTBOam5xNC9LRVdjYjJqbitEcUFFVkNYU3R6QUY0MQpKbnJnTDJsUHJhYzRCbjdBWFZtMnlocGZFbWRoSkJ1ZWtLMTdBYmxadk9nMk9ZZ0xnV1VUSklvM0VETFV6SzJhCmtjS2hpUFdiWGhPMSthZ1B1OGJZbXprd3NDU0RDYU9XUm9pc0wxNHRpRW1uUk9adUtlOVR3WUVaZjhhZW1BYzcKOG1DTW0rVUNnWUVBNnlUOHZsZ2dHYUFQK1hZdHk0K2Fmanl2MmM1MWNDSVZwQnkwaU54VzBWMEkxekNGbWpBYgpoZzRZRzBoM1hjNWlQUXpoaUFPdjJlOW04Z3JVZ3JCQzh3L0pGZzlzQzh1bDcwU2taMmFGVU10RHNyS1hVaFVOCllqQm9sOW83YTFjendGUXZzbTZDcGlWdzhEck0yUkVSNmpiWldCanNWdFJ0VndqUWc1SnJWU01DZ1lFQTBRaloKRGFtZjR1Wm84cjRaSWcwclVVR3BzemtVZ1VYV05vWFJ0clBYOS9pTExNUVUwVG9mejZIRjZvejQ5OFhIdXFPNQptZXBnVWg5Z2dUY3JXT1gwaG5VLy9jSHVsQlVGR1ltekZDTGxLK055WENHQjJIb3l0YTB3YXdNTG1NVDI1QnlUCllSNS85V0VjdWoyNzJ2TTBJNUdIdVlUcnlVVkFXRnpEMk1yQUNTc0NnWUErRkRlOXNOcFlxWTlPRnNza1VMZEMKMkphOGMrdEJQVjVsUTBiT1V3MkFuSVUyK3BKckVJUlRzZWpmV3hSWWFhSTU1K3FxMC81YkZQOGlxNFZMTTJtWgpiZk55cFlPcTRZcmZUa254R25uYnlLNEl3V2w5dkIwYWJSbFA3Y2QrSVRtQ2J6ZGdiaGlEOEtLT2FaZW5Qd0J4Cnl5VTZRdW95U2tiTGtZSXJHajdIN1FLQmdBRDUyNW1XdXlwaURRZGxIOVRxM3RMb1RaU0twYS9jMVowM2pyMGQKcGRxQWpKWE83SkgxaFlTQXF0M25mcG9aa0U4c3lJY1JjeFd2RTlKR29hc3VsNit4Q0hWMDNGcFFwczl4dHlaVwo2MThERnR1YlJISHdVQ0JOQ0xiWE1zeXB6c1ZONU5wRlBDKzNIbDA0UUpKTEpHa3VHc1lnK3NEYjArc1ZzOHhsClZmUTFBb0dCQU1VNnJaWUhEZG02anV0N3hDclFYYTU3eU5iSmRLVksydmZHS0g0VTFocytQdERaczExd3Z6bUkKM0hJTTFtVFJFNTJ6SWxyQjdsZzVZOXRmOTltWEJHSzJRV0QrVkhhMU5SZkxRWElJOEZCWnRxUkYyRHNGWUIzNwpOaXZpbnIxcVRybHc1ZElJOVVKUWNobUd2c20vQlBCRzlqUUZKSUpCRHpQWnRrS3JmaVU5Ci0tLS0tRU5EIFJTQSBQUklWQVRFIEtFWS0tLS0t"

    echo $SSH_KEY > /var/www/.ssh/id_rsa.base64
    base64 -d /var/www/.ssh/id_rsa.base64 > /var/www/.ssh/id_rsa
    chown www-data:www-data -R /var/www/.ssh
    chmod 600 /var/www/.ssh/id_rsa
fi

#Create user 
if id "www-data" >/dev/null 2>&1; then
    logger -t "$0" "User www-data already exists!"
else
    /usr/sbin/useradd -d /home/www-data -s /bin/false www-data &&\
    /usr/sbin/usermod -a -G sudo www-data
    logger -t "$0" "Create user www-data!"
fi

ARRAY_SITE=$( echo "${CONFIG_GIT_DEFAULT},${REPO2},${REPO3},${REPO4},${REPO5},${REPO6},${REPO7}" | sed 's/,/\n/g' )
echo "${ARRAY_SITE}" | while read SITE
do
    sudo -u www-data /bin/bash -c "cd /home/www; for repeat in {1..5}; do if git clone ${SITE}; then break;  else logger -t 'config_www' 'Repeat ${repeat} in 5, Error, clone repo ${SITE}'; sleep 10; fi; done;"
done
