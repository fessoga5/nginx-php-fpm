#!/bin/bash

if [ ! -f /.FIRSTSTART-SUCCESS ] ; then
	#Configure hosts
        HOSTNAME=$( cat /etc/hostname )
        FINDHOSTS=$( cat /etc/hosts | grep "${HOSTNAME}" -c )
        if [ "$FINDHOSTS" = "0" ]
        then
                echo "127.0.0.1 ${HOSTNAME}" >> /etc/hosts
                logger -t startup "Adding ${HOSTNAME} in /etc/hosts"
        fi

	echo "127.0.0.1" >> /etc/ansible/hosts

	# create DNS resolving
	DNS_NAME=${DNS:="8.8.8.8"}
	echo "nameserver ${DNS_NAME}" > /etc/resolv.conf

	# Get timezone
	TIMEZONE_SET=${TIMEZONE:="Asia/Irkutsk"}
	echo "${TIMEZONE_SET}" | tee /etc/timezone && dpkg-reconfigure --frontend noninteractive tzdata

	# Configure scripts
	./configure_site.sh
	
	# Create FIRSTART-SUCCESS
	touch /.FIRSTSTART-SUCCESS

fi

# Start supervisord and services
exec /usr/bin/supervisord -n -c /etc/supervisor/supervisord.conf
