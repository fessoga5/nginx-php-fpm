FROM ubuntu:14.04

MAINTAINER fessoga <fessoga5@gmail.com>

##Add repositories
RUN apt-get update && apt-get install -y --force-yes curl && \ 
    curl http://nginx.org/keys/nginx_signing.key | apt-key add - && \
    echo "deb http://nginx.org/packages/ubuntu/ trusty nginx" >> /etc/apt/sources.list && \
    echo "deb http://ppa.launchpad.net/ondrej/php/ubuntu trusty main" >> /etc/apt/sources.list && \
    echo "deb-src http://ppa.launchpad.net/ondrej/php/ubuntu trusty main" >> /etc/apt/sources.list && \
    echo "deb http://ppa.launchpad.net/git-core/ppa/ubuntu trusty main" >> /etc/apt/sources.list && \
    apt-get update

RUN apt-get install -y --force-yes bash \
    curl \
    openssh-client \
    openssh-server \
    fping \
    vim \
    sudo \
    wget \
    nginx \
    #nginx-module-geoip \
    supervisor \
    curl \
    git \
    rsyslog \
    python-yaml \
    sendmail  \
    nano \
    php7.0 \
    php7.0-curl \
    libjson-perl \
    php7.0-mysql \
    java-common \
    php7.0-pgsql \
    php7.0-mcrypt \
    php7.0-mbstring \
    php7.0-curl \
    #php7.0-dompdf \
    php7.0-gd \
    php7.0-mcrypt \
    php7.0-mysql \
    php7.0-sqlite \
    php7.0-geoip \
    php7.0-fpm \
    php7.0-mysql \
    php7.0-mcrypt \
    php7.0-gd \
    php7.0-intl \
    php7.0-memcache \
    php7.0-sqlite \
    php7.0-pgsql \
    php7.0-xmlrpc \
    php7.0-xsl \
    php7.0-curl \
    php7.0-json \
    php7.0-zip \ 
    apache2-utils \
    ansible \
    snmp \
    unzip \
    java-common \
    php7.0-pgsql \
    php7.0-mcrypt 
# PHP7-0 ssh2
RUN sudo apt-get install -y --force-yes libssh2-1-dev libssh2-1 php7.0-dev
RUN cd /opt && wget https://github.com/Sean-Der/pecl-networking-ssh2/archive/php7.zip && unzip php7.zip && \
    cd /opt/pecl-networking-ssh2-php7 && phpize && ./configure && make install clean


RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/bin --filename=composer && \
    mkdir -p /etc/nginx && \
    mkdir -p /var/www/app && \
    mkdir -p /run/nginx && \
    mkdir -p /var/run/sshd && \
    mkdir -p /var/run/php && \
    mkdir -p /home/www && \
    chown www-data:www-data -R /home/www && \
    chown www-data:www-data -R /var/run/php && \
    mkdir -p /var/log/supervisor 

# Add config for supervisord
ADD conf/supervisor /etc/supervisor/conf.d

# Copy our nginx config
ADD conf/nginx/nginx.conf /etc/nginx/nginx.conf
ADD conf/nginx/ngx_http_auth_request_module.so /etc/nginx/ngx_http_auth_request_module.so
ADD conf/nginx/boilerplate /etc/nginx/boilerplate/
ADD conf/nginx/sites-available /etc/nginx/sites-available/
ADD conf/nginx/sites-enabled /etc/nginx/sites-enabled/

# Logrotate
ADD conf/logrotate.d/nginx_custom_hosts /etc/logrotate.d/nginx_custom_hosts
ADD conf/logrotate.d/rsyslog /etc/logrotate.d/rsyslog

# Php7.1 configure fpm
ADD conf/fpm /etc/php/7.0/fpm/

#Copy startup script
ADD conf/docker-startup /opt/docker-startup/

#Add emptyindex
ADD emptyindex /opt/emptyindex/

#Copy deploy script
ADD scripts/configure_site.sh /configure_site.sh

# Add Scripts
ADD scripts/start.sh /start.sh

# Chmod 755 for starting sh scripts
RUN chmod 755 /*.sh

# Create user, adding for group. Working for ssh
RUN /usr/sbin/useradd -d /home/ubuntu -s /bin/bash -p $(echo gfhfljrc11 | openssl passwd -1 -stdin) ubuntu &&\
/usr/sbin/usermod -a -G sudo ubuntu

EXPOSE 443 80 

#CMD ["/usr/bin/supervisord", "-n", "-c",  "/etc/supervisord.conf"]
CMD ["/start.sh"]
